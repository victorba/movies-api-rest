const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const port = process.env.PORT || 3000;
const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('dev'));

app.listen(port,() => {
    console.log(`Listening on ${port}`);
})